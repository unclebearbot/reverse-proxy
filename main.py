#!/usr/bin/env python3

import sys

import argparse
import requests
from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn

global remote_scheme
global remote_host
global remote_port


def main():
    args = parse_args()
    global remote_scheme
    remote_scheme = args.remote_scheme
    global remote_host
    remote_host = args.remote_host
    global remote_port
    remote_port = args.remote_port
    scheme = 'http'
    host = '0.0.0.0'
    port = args.local_port
    print('{}://{}:{} -> {}://{}:{}'.format(scheme, host, port, remote_scheme, remote_host, remote_port))
    ThreadedHTTPServer((host, port), ProxyHTTPRequestHandler).serve_forever()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--local-port', dest='local_port', type=int, default=8080)
    parser.add_argument('--remote-scheme', dest='remote_scheme', type=str, default='http')
    parser.add_argument('--remote-host', dest='remote_host', type=str, default='example.com')
    parser.add_argument('--remote-port', dest='remote_port', type=str, default='80')
    return parser.parse_args(sys.argv[1:])


class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """"""


class ProxyHTTPRequestHandler(BaseHTTPRequestHandler):
    # noinspection PyPep8Naming
    def do_HEAD(self):
        self.do_GET(body=False)
        return

    # noinspection PyPep8Naming
    def do_GET(self, body=True):
        succeeded = False
        try:
            response = requests.get(
                '{}://{}{}'.format(remote_scheme, remote_host, self.path),
                headers=copy_with_update(self.get_headers(), new_headers()),
                verify=False
            )
            succeeded = True
            self.send_response(response.status_code)
            self.populate_headers(response)
            if body:
                self.wfile.write(response.content)
            return
        finally:
            if not succeeded:
                self.send_error(500, 'Error in reverse proxy')

    # noinspection PyPep8Naming
    def do_POST(self, body=True):
        succeeded = False
        try:
            response = requests.post(
                '{}://{}{}'.format(remote_scheme, remote_host, self.path),
                headers=copy_with_update(self.get_headers(), new_headers()),
                data=(self.rfile.read(int(self.headers.get('content-length', 0)))),
                verify=False
            )
            succeeded = True
            self.send_response(response.status_code)
            self.populate_headers(response)
            if body:
                self.wfile.write(response.content)
            return
        finally:
            if not succeeded:
                self.send_error(500, 'Error in reverse proxy')

    def get_headers(self):
        result = {}
        for header in self.headers:
            pair = [array.strip() for array in header.split(':', 1)]
            if len(pair) == 2:
                result[pair[0]] = pair[1]
        return result

    def populate_headers(self, response):
        for header in response.headers:
            if header not in [
                'Content-Encoding', 'content-encoding',
                'Content-Length', 'content-length',
                'Transfer-Encoding', 'transfer-encoding',
            ]:
                self.send_header(header, response.headers[header])
        self.send_header('content-length', '{}'.format(len(response.content)))
        self.end_headers()


def copy_with_update(a, b):
    result = a.copy()
    result.update(b)
    return result


def new_headers():
    return {'host': remote_host}


if __name__ == '__main__':
    main()
